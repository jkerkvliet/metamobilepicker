import yaml
import re
from sys import argv
from Bio import SeqIO

def validateIS(is_entry):
    """
    Function takes an entry containing an IS and it returns
    if the IS is worth putting in the output file. This decision
    is based on the number of IS (>1) in the entry and the length of the flanking regions of the IS (>200bp)
    """

    num_is = is_entry['contig']['number_IS']
  
    if num_is > 1:
        return True

    contig_length = is_entry['contig']['length']
    is_start = is_entry['contig']['IS_1']['start']
    is_stop = is_entry['contig']['IS_1']['stop']
    
    # checks if at least 200bp of non-IS is on the contig
    if contig_length - is_stop < 200 and is_start < 200:
        return False
    else:
        return True

def extractContig(entry, fastadict,outputfasta, mode):
    fastarecord =  fastadict[entry['contig']['contig_id']]
    mgelist = []
    openoutput = open(outputfasta,'a')

    # if plasmid is predicted add score to header
    if 'plasmid' in entry['contig'].keys():
        mgelist.append('plasmid')
        mgelist.append('plscore_{score:.4f}'.format(score=entry['contig']['plasmid']['score']))

    # if phage is predicted, add score to header
    if 'phage' in entry['contig'].keys():
        mgelist.append('phage')
        mgelist.append('phscore_{score:.4f}'.format(score=entry['contig']['phage']['score']))

    # if valid IS is found, add all IS to header
    if entry['contig']['number_IS'] > 0:
        for i in range(entry['contig']['number_IS']):
            isid = "IS_{}".format(i+1)
            isentry = entry['contig'][isid]
            mgelist.append('IS_{}_{}_{}'.format(isentry['ID'],isentry['start'],isentry['stop']))

    # if user requests AMR, it is added to the header
    if mode == "full" and entry['contig']['number_annotations'] > 0:
        for i in range(entry['contig']['number_annotations']):
            resid = "Annotation_{}".format(i+1)
            resentry = entry['contig'][resid]
            mgelist.append(f"{resentry['source']}_{resentry['gene']}_{resentry['start']}_{resentry['stop']}")
    
    # header is joined together
    fastasuffix = '|'.join(mgelist)
    newid = ">{}|{}".format(fastarecord.id,fastasuffix)

    # sequence is written to output file
    openoutput.write("{}\n{}\n".format(newid,fastarecord.seq))

def main(args):
    """
    The script outputs a fasta file containing either all MGE-contigs with AMR 
    or only the MGE containing contigs without AMR in the fasta header.
    """

    # mode is full by default, but can be set to MGE only
    mode = args[4] # either MGEs or full (with ARGs)
    if mode != "MGEs" and mode != "full":
        mode = "full"
 
    fastafile = args[2]
    fastadict = SeqIO.index(fastafile,"fasta")
    outputfasta = args[3]
    open(outputfasta,'w') # clears the file without warning!

    mmpout = open(args[1],'r').read()
    mmpout = re.sub(r"\ncontig", "\n---\ncontig", mmpout) # make the file yaml valid

    mmpdata = yaml.load_all(mmpout,Loader=yaml.FullLoader)

    mmpdict = {}    

    mmpdict = {x['contig']['contig_id']:x for x in mmpdata}
   
    # looping over it once more to verify and add to the header
    for contig in mmpdict:
        entry = mmpdict[contig]
        if entry['contig']['number_IS'] > 0:
            state = validateIS(entry)
            if state:
                extractContig(entry,fastadict, outputfasta, mode)

        # Select high scoring phages
        elif 'phage' in entry['contig'].keys() and not 'plasmid' in entry['contig'].keys():
            if entry['contig']['phage']['score'] >= 0.9:
                extractContig(entry,fastadict, outputfasta, mode)
            else:
                pass

        # If plasmid
        else:
            extractContig(entry,fastadict, outputfasta, mode)

if __name__ == "__main__":
    main(argv)
