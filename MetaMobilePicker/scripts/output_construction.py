#!/usr/bin/python
#SAMPLE_NAME="HPC_full_bm3"
import sys
sys.path.append('scripts/')
import output_classes

def getContigLength(contig):
    """
    Splits a MetaSpades contig on the length value and returns it
    """
    return contig.split("_")[3]

def getInformation(samplename,filename, type, outputdir):
    """
    Function takes a filename and an identifier. It reads the file and outputs the
    contents as a list. Extra operations are done depending on the identifier.
    Returns: list of parsed input data in list format
    """
    ## TODO: replace line below with workdir variable
    filename = ("{}/{}/{}".format(outputdir,samplename,filename))
    with open(filename) as f:
        results = f.readlines()
        if type == "plasmid":
            # Plasflow includes an empty column
            results[0] = "{}".format(results[0]) 
        elif type == "IS":
            # ISEScan uses a set of spaces rather than a tab
            # And includes a line of dashes between header and data
            results = results[2:]
            results = [x.replace(" ","\t") for x in results]

        # Removes all newline characters and splits into lists
        results = [x.strip("\n") for x in results]
        results = [x.split("\t") for x in results]

    return(results)


def plasclass(samplename, outputdir):
    """
    Takes a sample name. Function reads plasclass results,
    takes the contig name and score. Creates a contig object
    for every found plasmid. It then adds a plasmid object to this contig.
    Returns a dictionary with plasmid contigs and the contig objects
    """
    # Example of plasclass output:
    # NODE_1_....	0.84
    
    pc_filename = "{}_MGEs/{}_plasclass_plasmids.tsv".format(samplename,samplename)
    plasclass_results = getInformation(samplename, pc_filename,"plasmid", outputdir)
    information = [[x[0], getContigLength(x[0]), x[1]] for x in plasclass_results]
    objdict = {}
    
    for i in information:
        contig = output_classes.Contig(i[0],i[1])
        objdict[i[0]] = contig
    
        plobj = output_classes.Plasmid(name="plasmid", length=i[1], score=i[2])
        objdict[i[0]].addMGE(plobj)
        #plcounter += 1
    return(objdict)
   

def deepvirfinder(dvf_contigs,samplename, outputdir):
    dvf_filename="{}_MGEs/{}_phages.txt".format(samplename,samplename)
    dvf_results =  getInformation(samplename,dvf_filename, "Phage", outputdir)
    for i in dvf_results[1:]:
        try:
            length_i = dvf_contigs[i[0]].getLength() # See if contig object exists
        except KeyError:
            newContig = output_classes.Contig(i[0], getContigLength(i[0])) # Determine length based on fasta header
            dvf_contigs[i[0]] = newContig
            length_i = dvf_contigs[i[0]].getLength()
        phobj = output_classes.Phage(length=length_i, score=i[2])
        dvf_contigs[i[0]].addMGE(phobj)
    return(dvf_contigs)


def abricate(ab_contigs, samplename,database, outputdir):
    """
    Function takes dictionary with contig objects and adds ResFinder results
    Returns: dictionary with contig objects
    """
    # Example Abricate output
    # #FILE   SEQUENCE        START   END     STRAND  GENE    COVERAGE        COVERAGE_MAP    GAPS    %COVERAGE	%IDENTITY	DATABASE        ACCESSION	PRODUCT RESISTANCE
    ab_filename="{}_annotation/{}_{}.csv".format(samplename, samplename,database)

    abricate_results = getInformation(samplename, ab_filename, "Abricate", outputdir)
    # Retrieve useful information from file
    information = [[x[1],x[2],x[3],x[5],x[6],x[10],x[11],x[12],x[13]] for x in abricate_results]

    for i in information:
        try:
            exists = ab_contigs[i[0]].getLength() # See if contig object exists
            # self, start,stop, name, source, gene, accession
            # Create annotation object
            annobj = output_classes.Annotation(i[1],i[2],i[8],i[6],i[3],i[7])
            # Add Annotation object to contig objects
            ab_contigs[i[0]].addAnnotation(annobj)
            
        except KeyError:
            # Contigs not in the database are not added, they don't have MGEs
            pass
            
    return(ab_contigs)

def isescan(is_contigs, samplename, outputdir):
    """
    Function takes a dictionary of contig objects and adds ISEScan results to it.
    Returns a dictionary with updated ccontig objects
    """
    #seqID  family  cluster  isBegin  isEnd  isLen ncopy4is start1 end1 start2  end2 score irId irLen nGaps  orfBegin    orfEnd strand  orfLen   E-value ov tir


    is_filename = "{}_MGEs/{}_IS.out".format(samplename, samplename)
    is_results = getInformation(samplename, is_filename, "IS", outputdir)
    # Only select the filled columns (ISEScan uses a bunch of spaces)
    for i in range(len(is_results)):
        is_results[i] = [x for x in is_results[i] if x]
    # Only some columns are used
    information = [[x[0],x[1],x[2],x[3],x[4]] for x in is_results]

    for i in information:
        try:
            exists = is_contigs[i[0]].getLength() # Check if contig already in dict
        except KeyError:
            length = getContigLength(i[0])
            newContig = output_classes.Contig(i[0], length) 
            is_contigs[i[0]] = newContig
        # Create IS object
        isobj = output_classes.IS(i[3],i[4],i[1], i[2])
        is_contigs[i[0]].addMGE(isobj)

    return(is_contigs)

def generate_eggnog_structure(coords):
    # Only the first header is relevant
    oddheader = True
    coorddict = {}
    for i in coords:
        if i[0] == "#" and oddheader:
             name=i.split("=")[-1]
             name = name.strip("\n").strip('"')
             #print(name)
             oddheader=False
             coorddict[name] = {}
        elif i[0] == ">":
             coord = i[1:].strip("\n").split("_")
             coorddict[name][coord[0]] = coord[1:3]
        else:
             oddheader=True
    return(coorddict)





def eggnog(egn_contigs, samplename):
    # TODO: add eggnog implementation
    egn_filename = "{}_annotation/{}_eggnog_genes.emapper.annotations".format(samplename, samplename)
    egn_coords_fn = "../results/{sample}/{sample}_annotation/{sample}_genes_coords.txt".format(sample= samplename)

    with open(egn_coords_fn,'r') as cf:
        egn_coords = cf.readlines()
    #print(egn_coords[0])
    coorddict = generate_eggnog_structure(egn_coords)

    # Query_name, seed eggNOG ortholog, seed ortholog evalue, seed ortholog score, Predicted taxonomic g$
    # Predicted protein name, Gene Ontology terms, EC number, KEGG_ko, KEGG_Pathway, KEGG_Module, KEGG_R$
    # KEGG_rclass,BRITE,KEGG_TC, CAZy , BiGG Reaction, tax_scope: eggNOG taxonomic level used for annota$
    # eggNOG OGs, bestOG (deprecated, use smallest from eggnog OGs), COG Functional Category, eggNOG fre$
    egn_results = getInformation(samplename, egn_filename, "Eggnog")
    #print(egn_results[1])
    # contig, taxgroup, protein name, GO, EC
    information = [[x[0],x[4],x[5],x[6],x[7]] for x in egn_results]
    for i in information:
        #print(i)
        try:
            name = '_'.join(i[0].split("_")[:-1])
            gene = i[0].split("_")[-1]
            length = egn_contigs[name].getLength() # Check if contig exists
            #print(name, coorddict[name][gene], length)
            startstop = coorddict[name][gene]
            start  = int(startstop[0])
            stop = int(startstop[1])
            #start, stop = 1,1
            # self, start,stop, name, source, gene, accession
            egnobj = output_classes.Annotation(start=start, stop=stop, name=i[1], source="Eggnog", gene=i[2], accession=i[4])
            # Add Annotation object to contig objects
            egn_contigs[name].addAnnotation(egnobj)

        except KeyError:
            pass # We don't need annotation of non-MGE contigs
    return(egn_contigs)

def make_output(contigs, samplename):
    """
    Function takes the finalized contig dictionary and creates the output
    hierrarchy by calling the hierarchy method of each object
    """
    #NODE_1_length_385042_cov_52.418386
    contignames = list(contigs.keys())
    contignames.sort(key=lambda item:(int(item.split("_")[1])))
    for i in contignames:
        obj = contigs[i]
        hierarchy = obj.printHierarchy()
        if hierarchy != None:
            print(hierarchy)

def main(args):
    """
    Main function calls all MGE parsing functions and creates the
    hierarchical output
    """ 
    samplename = args[1]
    outputdir = args[2]
    contigs = plasclass(samplename, outputdir)
    dvf_contigs = deepvirfinder(contigs, samplename, outputdir)
    is_contigs = isescan(dvf_contigs, samplename, outputdir)
    ab_contigs = abricate(is_contigs, samplename,"resfinder", outputdir)
    mr_contigs = abricate(ab_contigs, samplename,"megares", outputdir)
    #egn_contigs = eggnog(ab_contigs, samplename)
    make_output(mr_contigs, samplename)
    
if __name__ == "__main__":
    args = sys.argv
    main(args)


