#!/usr/bin/python
import yaml
import click
import subprocess
import logging
import os
import urllib.request
from shutil import copyfile
from datetime import date
from collections import defaultdict
from pathlib import Path
from rich import print
PATH = os.path.dirname(os.path.realpath(__file__))
from .version import version as VERSION


"""
Start script for the MetaMobilePicker Pipeline. 
This script uses the user supplied parameters to start the pipeline.
"""
def replaceAssembly(configfile,fastapath, assemblyfile):
    print(assemblyfile)
    # First check how many samples are supplied
    with open(configfile,'r') as stream:
        try:
            conffile = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            exit(1)
    samples = list(conffile['samples'].keys())
    numsamples = len(samples)
    
    # Second check how many assembly files are specified
    if assemblyfile and not fastapath:
        with open(assemblyfile,'r') as assemblies:
            fastanames = assemblies.readlines()
            fastanames = [x.strip("\n") for x in fastanames]
    elif fastapath and not assemblyfile:
        fastanames = [fastapath]    
    elif fastapath and assemblyfile:
        print("Please either supply a single fasta file or a file with fasta files listed")
        exit(1)
     
    # Third check if any assembly files already exist
    if len(fastanames) != numsamples:
        print("The number of assemblies you specified isn't equal to the numbers of samples you want to run")
        print("Please make sure these numbers match. Is there a blank line in your assembly file?")
        exit(1)
 
    for i in range(len(samples)):
        # verify if exists
        sample = samples[i]
        assembly = fastanames[i]
        path = f"{PATH}/data/{sample}/userdata/contigs.fasta"
        if os.path.exists(path.format(sample=sample)):
            print("The assembly file you want to replace for sample {} already exists. I don't dare to overwrite it!".format(sample))
        else:
            os.makedirs(os.path.dirname(path.format(sample=sample)),exist_ok=True)
            copyfile(assembly,path.format(sample=sample))
        
    
# Fourth copy the assembly files to their correct location
    
def doCheckTest(test,config):
    """
    Checks if the test flag is used and if the test files are in the correct location.
    Returns the config file based on if test mode is activated or not
    DEPRECATED: a small test file is now included with MMP
    """
    #file_2 = f"{PATH}/test/ERR2984773_2_test.fastq.gz"  
    #file_1 = f"{PATH}/test/ERR2984773_1_test.fastq.gz"
    #if test and os.path.exists(file_2) and os.path.exists(file_1):
    if test:
        config = f"{PATH}/config/test.yaml"
    #if test and (not os.path.exists(file_2) or not os.path.exists(file_1)):
    #    print("You haven't downloaded any test data. Please run 'metamobilepicker.py download' first")
    #    exit(1)
    return config

# DEPRECATED, to be removed
#def getRunName(outdir, callname="demessifier"):
#    """
#    Returns a runName based on today's date and generates a unique name if today's date is already taken.
#    """
#    if not os.path.isdir("{}/{}".format(outdir,callname)):
#        return(callname)
#
#    today=date.today()
#    default_name="{}_{}_{}_{}".format(callname, today.year,today.month,today.day)
#    count = 1
#    outname = default_name
#    while True:
#        if os.path.isdir("{}/{}".format(outdir,outname)):
#            outname = "{}_{}".format(default_name,count)
#            count += 1
#        else:
#            break
#
#    return(outname)
#

def goodbye():
    """
    Returns exit message.
    """
    print("Thanks for using [blue3]MetaMobilePicker![/blue3]")

# Add date and time to log messages
logging.basicConfig(
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M",
    format="[%(asctime)s %(levelname)s] %(message)s",
)

# Sets up click groups for input/flag handling
@click.group(invoke_without_command=True)
@click.version_option(VERSION)
def cligroup():
    logo = Path(f'{PATH}/logo/logo.txt').read_text()
    print(f"{logo}")
    print("[bold blue3]Identification of Mobile Genetic Elements in metagenomics samples[/bold blue3]")
    #"""
    ##### MetaMobilePicker #####
    #
    #Identification of Mobile Genetic Elements in metagenomics samples.
    #"""

        
# Sets up the click group for the 'download' module
@cligroup.command(
    "download",
    short_help="Download test data and Eggnog database"
)

## DEPRECATED, to be removed
#@click.option(
#    "-d","--database",
#    is_flag=True,
#    help="Download the necessary database"
#)

@click.option(
    "-t","--testdata",
    is_flag=True,
    help="Download test dataset"
)

def download_data(testdata, database=False):
    """
    Downloads the necessary database and/or test data to run the analysis.
    """
    if not database and not testdata:
        testdata = True
        database = True

    print("Downloading files, this might take a while")
    if testdata:
        file_1 = f"{PATH}/test/ERR2984773_1_test.fastq.gz"
        file_2 = f"{PATH}/test/ERR2984773_2_test.fastq.gz"
        # download test data
        if not os.path.exists(file_1):
            print("Downloading testfile 1")
            url_1 = "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR298/003/ERR2984773/ERR2984773_1.fastq.gz"
            urllib.request.urlretrieve(url_1, file_1)
        if not os.path.exists(file_2):
            url_2 = "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR298/003/ERR2984773/ERR2984773_2.fastq.gz"

            print("Downloading testfile 2")
            urllib.request.urlretrieve(url_2, file_2)
        else:
            print("Test data is already downloaded")

    """
    # Downloading the eggnog database doesn't really work. Eggnog has it's own script for it, and 
    # it requires the full package to be installed as dep. I don't think I want that. Ideas welcome ;)
    # Also, the db is already downloaded in the main run, it's just nice to be able to do it 'manually'
    # too, to avoid doing it in runtime.
    if database:
        if not os.path.exists("../data/raw/eggnog2/eggnog.db"):
            print("Downloading database")
            subprocess.call(["python","./scripts/download_eggnog_data.py","--data-dir","../data/raw/eggnog2/","-y"])
        else:
            print("Database already exists")         
    """

## CLI-GROUP config
@cligroup.command(
    "config",
    short_help="Initiate MetaMobilePicker by generating a config file"
)
@click.option(
    "-s","--samples",
    default="samples.txt",
    show_default=True,
    help="Supply a custom text file with sample names and read locations"
)
@click.option(
    "-a","--assembly",
    help="Supply custom assembly for all samples in samples.txt"
)
@click.option(
    "-o","--output",
    help="Name of the config file",
    default="config.yaml"
)
@click.option(
    "-t","--threads",
    help="Maximum number of threads to use",
    default=16,
    show_default=True
)
@click.option(
    "-h","--host",
    help="Fasta file with host sequences",
    default="no_host",
    show_default=False
)
@click.option(
    "-m","--memory",
    help="Set the maximum available memory (make sure to have enough for your assembly!)",
    default=8,
    show_default=True
)
@click.option(
    "-d","--datadir",
    help="Set the path to your data",
    default=os.getcwd(),
    show_default=True
)
@click.option(
    "-O","--outdir",
    help="Set the path to your output directory",
    default=os.getcwd(),
    show_default=True
)
def makeConfig(samples,memory, threads,output, datadir,outdir,host,assembly=None):
    # Verify if samples file exists
    assert os.path.exists(samples), "Samples file not found, please check your file"
    sampledict = {}
    sampledict["samples"] = defaultdict(dict)

    # Read samples file, split lines
    with open(samples,'r') as samplefile:
        slist = samplefile.readlines()
        slist = [x.split(",") for x in slist]
    
    # Add forward and reverse paths to sampledict 
    for s in slist:
        sampledict["samples"][s[0]]['fwd'] = "{fwd}".format(fwd=os.path.abspath(s[1]))
        sampledict["samples"][s[0]]['rev'] = "{rev}".format(rev=os.path.abspath(s[2].strip("\n")))

    sampledict["samples"] = dict(sampledict["samples"]) # defaultdicts mess with yaml output
    
    # Add threads configuration based on maximum number of threads provided
    sampledict["threads"] = {"small":min(threads,4),"medium":min(threads,8),"big":int(threads/2),"huge":threads}

    # Check if provided host_db is in correct position
    if host=="no_host":
        host=f"{PATH}/test/no_host.fna"
        print("WARNING: a host sequence file wasn't specified. I'll use a mock sequence file, but consider adding a real host file!")
    assert os.path.exists(host),"Host file doesn't exist"
    sampledict["host"] = os.path.abspath(host)    

    sampledict["max_mem"] = memory
    if output =="config.yaml":
        print("WARNING: No output file was specified, it will be written to config.yaml. Please make sure that's okay!")
    
    # Make sure the config has a yaml extension
    if not output.endswith(".yaml") and not output.endswith(".yml"):
        output = f"{output}.yaml"

    # Add outputdir and datadir
    outdir = os.path.abspath(outdir)
    datadir = os.path.abspath(datadir)
    outdir = outdir.replace("~",str(Path.home()))
    datadir = datadir.replace("~",str(Path.home()))

    assert os.path.isdir(datadir), "The general data directory does not exist. I don't dare create it!"
    assert os.path.isdir(outdir), "The output directory does not exist. I don't dare create it!"
    
    sampledict["datadir"] = datadir.rstrip("/")
    sampledict["outdir"] = outdir.rstrip("/")
    # Write dictionary to yaml file
    with open(output, 'w') as file:
        documents = yaml.dump(dict(sampledict), file)

## CLIGROUP running the pipeline
# Sets up the click group for the 'run' module
@cligroup.command(
    "run",
    short_help="Run MetaMobilePicker"
)
@click.option(
    "-n","--dryrun",
    is_flag=True,
    default=False,
    show_default=True,
    help="Test the script without running the pipeline"
)
@click.option(
    "-a","--assembly",
    default=None,
    help="Run MetaMobilePicker with a preexisting assembly file"
)
@click.option(
    "-A","--assemblyfile",
    default=None,
    help="Run MetaMobilePicker with a set of preexisting assemblies"
)
@click.option(
    "-s","--snakefile",
    default="Snakefile",
    show_default=True,
    help="Change the name of the Snakefile to be used"
)

@click.option(
    "-p","--profile",
    default=None,
    help="Use Snakemake profile"
)
@click.option(
    "-c","--config",
    default="config/config.yaml",
    show_default=True,
    help="Specify config file"
)
# DEPRECATED, to be removed
#@click.option(
#    "-s","--sbatch",
#    is_flag=True,
#    default=False,
#    show_default=True,
#    help="Run MetaMobilePicker on a SLURM cluser (BETA)"
#)
@click.option(
    "-u","--unlock",
    is_flag=True,
    default=False,
    show_default=False,
    help="Unlock directory after failed Snakemake attempt"
)
@click.option(
    "-t","--test",
    is_flag=True,
    default=False,
    show_default=False,
    help="Run MetaMobilePicker with a small test set"
)

# DEPRECATED, to be removed
#@click.option(
#	"-S","--submit",
#	is_flag=True,
#	default=False,
#	show_default=False,
#	help="Submit the snakemake script to SLURM"
#)

@click.option(
	"-C","--cores",
	default=16,
	show_default=True,
	help="Specify the number of available cores"
)

def run_pipeline(dryrun, profile, config, snakefile, test, unlock, assembly, assemblyfile, cores):
    """
    Runs the MetaMobilePicker pipeline.
    """

    config = doCheckTest(test, config)
    if assembly or assemblyfile:
        replaceAssembly(config, assembly,assemblyfile)


    # Base command
    cmd = (
        f"snakemake --use-conda --snakefile {PATH}/{snakefile} --cores {cores} \
        --configfile {config} --rerun-incomplete  --nolock --latency-wait 60 --directory {PATH} --use-singularity\
        --singularity-prefix {PATH}/.snakemake --singularity-args '--home $PWD/.. --bind $TMPDIR,$PWD/..'"
        )

    if dryrun:
        cmd += " --dryrun"
    if unlock:
        cmd += " --unlock"

    if profile != None:
        cmd += f" --profile {profile} "

    # DEPRECATED, to be removed
    #if sbatch:
    #    cmd += ' --profile ../slurm/'
    #    #cmd += ' --cluster "sbatch -t 18:00:00 -N 1 -c 20 --mem-per-cpu 12G --verbose"'
    #    runname = getRunName("../docs/log", callname="MetaMobilePicker_slurms")

    #if sbatch and submit:
    #    cmd = "sbatch -J MetaMobilePicker -t 48:00:00 -c 1 --mem-per-cpu 16G --gres=tmpspace:64G  {}".format(cmd)
    #elif submit and not sbatch:
    #    logging.critical("--submit can only be used in combination with --sbatch")
    #    exit(1)
    
    # Run the pipeline
    try:
        subprocess.check_call(cmd,shell=True)
        goodbye()
        exit(0)
    except subprocess.CalledProcessError as error:
        logging.critical(error)
        exit(1)

def main():
    cligroup()

if __name__ == "__main__":
    main()
