.. MetaMobilePicker documentation master file, created by
   sphinx-quickstart on Wed Dec 22 10:29:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   testmd.md
   :maxdepth: 2
   :caption: Contents:


Validation of MetaMobilePicker
==============================

In order to validate the results of the pipeline, we constructed a simulated metagenomic dataset. We selected the genomes of 7 highly resistant bacterial species from the PATRIC database (:cite:t:`wattam_patric_2014`) (accessed on March 3rd 2021). For this dataset we selected a representative completed genome from *E. coli*, *S. aureus*, *E. faecalis*, *M. tuberculosis*, *S. enterica*, *K. pneumoniae* and *A. baumanii*. 

Strains, genome accession IDs are found in Table 1. In addition to these genomes, five phage genomes associated with the respective species were selected. See Table 1 for an overview of the selected bacterial genomes, plasmids and phages. 

Using the selected genomes, we simulated a dataset of 20M reads of 150bp using InSilicoSeq (:cite:t:`gourle_simulating_2019`) using the MiSeq error profile. In order to simulate different relative abundances of different genomes, we generated abundance profiles for all species using a lognormal distribution. Additionally, a copy number for the plasmids was determined using a geometric distribution with probability P = min(1,log10(length)/7), to simulate shorter plasmids having a higher probability of having a higher copy number than longer plasmids. For the phages, a similar approach was used, but with a probability P = min(1,log10(length)/5)) to compensate for the lower average genome size of the phages. This copy number was multiplied by the abundance of the corresponding genome.
The complete genome assemblies, plasmids and phages were annotated using Abricate with the ResFinder (:cite:t:`florensa_resfinder_2022`) database to inventorize AMR genes present. Additionally, ISEScan (:cite:t:`xie_isescan_2017`) was used to identify the present IS sequences.

To backtrace the contigs assembled during the pipieline’s run associated with the plasmids and phages, the reads used in the assembly were mapped onto the assembled contigs. InSilicoSeq retains the identifier of the original sequence as part of the read header. These headers were counted per contig and plasmid and phage contigs were identified using a majority vote. Contigs with inconclusive molecular origin (i.e. a mix of phage, plasmid and chromosome), were manually checked and labeled ambiguous if no suitable conclusion could be reached. 

.. list-table:: Table 1: selected species for simulated validation data
   :widths: 20 20 20 20 20
   :header-rows: 1

   * - Accession
     - Species
     - Strain
     - No. plasmids
     - Phage accession
   * - CP034123.1 
     - Klebsiella pneumoniae 
     - BJCFK909
     - 4
     - NC_025418.1
   * - NC_021733.1 
     - Acinetobacter baumannii 
     - BJAB0715
     - 1
     - NC_031117.1
   * - NZ_CP017593.1 
     - Mycobacterium tuberculosis
     - Beijing-like/35049
     - 0 
     - JF937092.1
   * - NZ_CP018642.1 
     - Salmonella enterica 
     - 74-1357
     - 1
     - NA
   * - NZ_CP027788.1 
     - Staphylococcus aureus 
     - CMRSA-6
     - 0
     - NC_016565.1
   * - NZ_CP041877.1 
     - Enterococcus faecalis 
     - SCAID PHRX1-2018
     - 1
     - NC_023595.2
   * - NZ_CP044148.1 
     - Escherichia coli O157 
     - AR-0427
     - 2
     - NA
   
     
Using MetaSPAdes (:cite:t:`nurk_metaspades_2017`) as part of MetaMobilePicker, the 20M reads were assembled into contigs. This resulted in 1513 contigs, of which 887 larger than 1Kb with an N50 of 82,245 and an L50 of 97. The total assembly length accumulated to 29,747,115 bases.

Plasmid identification
++++++++++++++++++++++

The resulting contigs were filtered on 1Kb length, and in parallel ran through the MGE identification part of MetaMobilePicker. Plasmid sequences were identified with a cut-off classification score of 0.8. This resulted in 111 contigs predicted as plasmids. The predictions of PlasClass (:cite:t:`pellow_plasclass_2020`) were cross-referenced with the origin of each contig. The resulting confusion table is shown in Table 2. 

.. list-table:: Table 2: Plasmid classification confusion matrix
   :header-rows: 1


   * -
     - Predicted plasmids
     - Predicted non-plasmid
   * - Plasmids
     - 65
     - 19
   * - Non-plasmid
     - 46
     - 776

65 contigs were correctly predicted as plasmids, and 776 contigs were correctly predicted as chromosomal. 46 and 19 contigs were wrongly predicted as plasmid and chromosomal, respectively. This produces a precision and recall of 0.59 and 0.77, respectively and an F1 score of 0.67. Investigating the origin of each prediction class shows an overrepresentation of *E. coli* chromosomal fragments in the plasmid-prediction class. Of the 46 false positives, 30 originated from the *E. coli* genome. This overrepresentation might be due to a combination of factors. Comparing the length of each prediction class shows chromosomal contigs predicted as plasmids (FP) are on average shorter than chromosomal contigs not predicted as plasmids (TN) [p < 0.01]. In addition to this, we see that contigs originating from the *E. coli* genome are on average shorter than the contigs of most other species, with the exception of *E. faecalis*, possibly explaining part of the *E. coli* overrepresentation. An additional factor can be an algorithmic bias from PlasClass based on the abundance of *E. coli* in known plasmids. The length of the *E. faecalis* contigs are not significantly different to those of *E. coli*, yet *E. faecalis* accounts for only one FP. This species is less often found in plasmid reference databases, suggesting an explanation for this difference.

Insertion Sequences
+++++++++++++++++++

To detect IS elements in the simulated metagenome validation data, we run the ISEscan module of MetaMobilePicker on the 887 contigs longer than 1kb. In total, 241 IS in 144 metagenome contigs were identified compared to 417 IS in the reference genomes, resulting in  an accuracy of 58.5%. To test if all IS elements in the reference genomes were represented in the metagenomic contigs, we then mapped the metagenomics contigs to the reference genomes. This showed that on 317 IS (76.6%)  annotated in the reference genomes a corresponding metagenomics contig was mapped whereas a metagenomic contig  was missing entirely for 100 IS present in the reference genomes (23.4%). Of the 317 IS with a corresponding metagenomics contig, 88 (27.8%) corresponded to 18 metagenomics contigs which mapped to multiple IS in the reference genomes. These 18 metagenomic contigs are therefore likely to represent assembly collapses. Furthermore, 8 IS annotated in the reference genomes (2.5%) corresponded only partially to a metagenomics contig. The remaining 221 IS in the reference genome (69.7%) corresponded to a contig uniquely mapped to that IS in the metagenomic contigs. 
 
To contrast the detection accuracy of ISEscan before and after metagenome assembly, we also calculated the maximum achievable amount of IS elements detection in the metagenome assembled contigs. To this end we discounted all IS elements that were lost or only partially assembled during metagenome assembly, by mapping all contigs back to the IS elements identified in the reference genomes, as described above. When taking only the 221 IS correctly mapping, the 18 contigs of the 88 ambiguously mapping IS elements, and the 8 partial mappings into account, which is all the IS contigs ISEScan had access to, ISEscan detected 227 and achieved an accuracy of 90.85%.

 
Bacteriophages
++++++++++++++
In order to measure the performance of DeepVirFinder (:cite:t:`ren_identifying_2020`) on our metagenomics assembly, we cross-referenced the contigs predicted by DeepVirFinder with a classification score greater than 0.95 with the contigs that originated from the five phages in the dataset. This analysis shows 27 predictions, of which 5 originate from the phages added to our community. These 5 phage contigs are the full-length assemblies of these phages. Of the 22 contigs not originating from our added phages, 14 have a direct link to phage DNA, most likely originating from prophages. Another 4 contigs are putatively linked to phages, containing hits not exclusively associated with phages. The remaining 4 contigs show no clear link to phage DNA. Since we are unable to determine the number of prophage or phage related genes not identified by DeepVirFinder, we don’t take the 18 phage-related genes into account when calculating the classification metrics. This resulted in a recall of 1.0, precision of 0.555 and an F1 of 0.713.

AMR identification
++++++++++++++++++

To test the performance of the AMR identification steps, we annotated the validation genomes using the ResFinder database and cross-referenced the metagenomic hits with the genomic hits. Of the 55 predicted AMR genes in the reference genomes, 41 were identified correctly in the correct genome. For 11 of the remaining 14 hits, we found the gene but not on the correct genome. These 11 hits comprised 7 unique AMR genes. The remaining 3 hits comprising 3 unique AMR genes were not found in the metagenome. Further investigation of the 7 genes that were identified but not in all the correct genomes shows that 6 of the genes were only identified once in the metagenomic assembly. Of these 6 contigs, 4 have less than 75% of the reads mapped to the contig originating from one genome. For all 4 ambiguous contigs, a sizable part of the mapped reads originate from the genomes where the AMR gene should have been identified. This shows that, similar to the IS, these genes were collapsed during the metagenomics assembly step.



References
++++++++++


.. bibliography::
