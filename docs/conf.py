extensions = ['sphinxcontrib.bibtex','sphinx.ext.autosectionlabel']
bibtex_bibfiles = ['refs.bib']
bibtex_reference_style  = 'author_year'

project = "MetaMobilePicker"
html_logo = "res/MMP_logo.png"

autosectionlabel_prefix_document = True
