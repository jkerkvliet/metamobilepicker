.. MetaMobilePicker documentation master file, created by
   sphinx-quickstart on Wed Dec 22 10:29:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _description:

General overview
++++++++++++++++

.. image:: res/workflow.png
  :alt: Overview of MetaMobilePicker


MetaMobilePicker identifies mobile genetic elements (MGEs) and annotates antimicrobial resistance genes (ARGs) from metagenomic short reads.
MetaMobilePicker can be roughly divided into three modules: Preprocessing, MGE Identification, Annotation and Output Construction. 

Preprocessing
+++++++++++++
During the preprocessing steps, the metagenomic short reads are subjected to quality control (QC) and metagenomics assembly.
QC is done using the QC Module of Metagenome Atlas (:cite:t:`kieser_atlas_2020`). This tool performs deduplication, quality trimming and host contamination removal.
Metagenomics assembly is done using MetaSPAdes (:cite:t:`nurk_metaspades_2017`). Contigs larger than 1000 bp are being used for the following modules.

MGE Identification
++++++++++++++++++
During the MGE identification step, plasmids, phages and insertion sequences (IS) are identified using PlasClass (:cite:t:`pellow_plasclass_2020`), DeepVirFinder (:cite:t:`ren_identifying_2020`) and ISEScan (:cite:t:`xie_isescan_2017`) respectively. For plasmids, contigs with a plasmid score larger than 0.8 are retained as plasmids. For phages, contigs with a score larger than 0.95 are retained as phage. For IS, default annotation parameters are used.


Annotation
++++++++++
During the Annotation steps, Abricate (https://github.com/tseemann/ABRicate) is used to annotate ARGs using the ResFinder (:cite:t:`florensa_resfinder_2022`) and MEGARES (:cite:t:`doster_megares_2020`) databases. 
Additionally, as a precursor for further functional annotation, Prodigal (:cite:t:`hyatt_prodigal_2010`) is used for gene prediction.

Output Construction
+++++++++++++++++++
During the output construction steps, custom scripts are used to create a YAML file containing the combined output of the previous modules.
Additionally a fasta file containing only the predicted MGEs is created with the annotation information in the fasta headers.
Contigs containing IS are filtered where contigs containing less than 200bp outside the IS are discarded.
Using BWA (:cite:t:`li_fast_2009`), the quality controled reads are aligned to the fasta containing the MGEs. The resulting bam file is used to create a profile and contig db using Anvi'o (:cite:t:`eren_community-led_2021`) for further visualization and annotation. By default, Anvi'o uses hierarchical clustering on the contigs to make a visualizable dendogram. This should usually not create issues due to the limited number of MGEs in a sample. However, if it does cause issues, please create a GitLab issue.

Note on processing power
++++++++++++++++++++++++
MetaMobilePicker is a relatively heavy pipeline, since it includes computationally demanding steps such as metagenomics assembly. We therefore recommend to run the pipeline on an HPC or a large server that is otherwise capable of running a metagenomics assembly, with a minimum of 64GB of RAM. MetaMobilePicker is as parallel as possible, using Snakemake (:cite:t:`molder_sustainable_2021`) as a job distribution backbone, and facilitates the use of Snakemake profiles for HPC job submission, but theoretically the tool could run on a low number of threads. 

Benchmarking data
+++++++++++++++++

To get an idea of the time and resources required to run MetaMobilePicker efficiently, we ran the tool on 20 metagenomics samples with 40M reads. Below are the average time and resource usages. Using higher numbers of CPUs can decrease the amount of time it takes to run a sample, but can lead to an increase of memory usage.

.. list-table:: Table 1: Per step resource usage
   :header-rows: 1


   * - Tool
     - Provided cores
     - Memory usage
     - CPU time (h:m)
   * - ATLAS QC
     - 16
     - 50G
     - 6:19
   * - MetaSPAdes
     - 24
     - 123G
     - 9:46
   * - PlasClass
     - 16
     - 19.7G
     - 0:6
   * - DeepVirFinder
     - 16
     - 30G
     - 5:19
   * - ISEScan
     - 16
     - 500M
     - 12:44


.. _references:
References
++++++++++

.. bibliography::

