.. MetaMobilePicker documentation master file, created by
   sphinx-quickstart on Wed Dec 22 10:29:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
.. toctree::
   :maxdepth: 1
   :caption: Quickstart guide 
   :hidden:

   quickstart

.. toctree::
   :caption: Full Description
   :hidden:
   :maxdepth: 1

   description

.. toctree::
   :maxdepth: 1
   :caption: Validation
   :hidden:

   validation


MetaMobilePicker
================

Welcome to the MetaMobilePicker documentation!
----------------------------------------------

MetaMobilePicker is a Snakemake pipeline designed to identify mobile genetic elements (MGEs), specifically plasmids, insertion sequences (IS) and phages, and antimicrobial resistance (AMR) genes in metagenomics samples.
It runs preprocessing steps, metagenomics assembly, several MGE identification tools, AMR database lookups and combines the output together into one yaml file.
Additionally, it creates two essential Anvi'o files for visualization purposes.

Getting started
---------------
Head over to the :ref:`quickstart` section to get started!
For the full description of the pipeline, head over to the :ref:`description` section.
For the source code, go to https://gitlab.com/jkerkvliet/metamobilepicker

Citing
------
MetaMobilePicker is currently unpublished, when it is citable, it will show up here.

When using MetaMobilePicker, consider also citing the tools included in the pipeline:
:ref:`Description:References`



