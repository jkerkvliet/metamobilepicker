from setuptools import setup, find_packages

with open('MetaMobilePicker/envs/requirements.txt') as file_open:
     requirements = file_open.read().splitlines()

setup(
    name="MetaMobilePicker",
    setup_requires=[
        "setuptools>=38.6.0",
        "setuptools_scm",
        "setuptools_scm_git_archive",
    ],
    use_scm_version={"write_to":"MetaMobilePicker/version.py"},
    #version="1.1.2-beta",
    #scripts=["gplas/snakefiles/mlplasmidssnake.smk"],
    packages=find_packages(),
    install_requires=requirements,
    include_package_data=True,
    package_data={'': ['MetaMobilePicker/*']},
    entry_points={
        'console_scripts': [
            'metamobilepicker = MetaMobilePicker.metamobilepicker:main',
            #'start = gplas.__main__:start',
            #'dostart = gplas:start'
            ],
    }

)



