[![Documentation Status](https://readthedocs.org/projects/metamobilepicker/badge/?version=latest)](https://metamobilepicker.readthedocs.io/en/latest/?badge=latest)
   

# MetaMobilePicker

<img src="MMP_logo.png" width="150px" style="display: block; margin: auto;" />


Version 0.7.2

Pipeline identifying MGEs in metagenomic data
## Workflow
MetaMobilePicker identifies Mobile Genetic Elements (MGEs) in metagenomics reads. To do this the following tools are run:
- The QC module of [Metagenome-ATLAS](https://github.com/metagenome-atlas/atlas) on the supplied reads
- [MetaSPAdes](https://github.com/ablab/spades) is used for metagenome assembly
- [PlasClass](https://github.com/Shamir-Lab/PlasClass) is run for plasmid classification
- [ISEScan](https://github.com/xiezhq/ISEScan) for IS identification
- [DeepVirFinder]() for bacteriophage identification
- [Abricate]() with the [ResFinder]() database to annotate antibiotic resistance genes
- After read remapping to the assembly with [Samtools](), [Anvi'o](https://github.com/merenlab/anvio) is used to generate a contig and profile db, to prime downstream analyses 

The full documentation can be found at [metamobilepicker.nl](metamobilepicker.nl)

## Installation
The best way to install MetaMobilePicker is inside a conda environment. Create a new conda environment using the command:
```
conda create --name metamobilepicker python=3.10
conda activate metamobilepicker
```

### Installing using mamba (recommended)
Next, install MetaMobilePicker using the following command
```
conda install -c conda-forge mamba
mamba install -c bioconda metamobilepicker
conda install -c bioconda biopython
```
*Note: At this point in time, biopython needs to be installed manually.*


### Installing using source code
```
git clone https://gitlab.com/jkerkvliet/metamobilepicker.git
cd metamobilepicker
pip install .
```

*Note: For running the pipeline, you will need Singularity (v3.8 or later) installed on your system.*

## Testing the Pipeline
### Technical test
After installation, the fastest way to test the installation is to use the included test data. This dataset consists of 5.000 reads and should run relatively fast.
To test the pipeline, run the following commands:
```
metamobilepicker run --test --dryrun
```
If this doesn't give any errors, run the pipeline with the following command
```
metamobilepicker run --test
```
If this is the first run of the pipeline, it will create the appropriate conda environments and download the used containers, which can take a while.

### Testing a run from scratch
To make sure everything is working as intended, you can create a new run using the same test data.

#### Config files
MetaMobilePicker is a Snakemake pipeline that works with YAML config files. The easiest way is to let MetaMobilePicker generate it's own config file. Before we can do this, we need to make the samples file. This comma seperated file contains your samples and the paths to the paired end reads.

Before we make the config file, copy the test reads to a location where you can easily locate them.
In your prefered directory run the following commands:
```
mkdir mmp_data
cp {PATH TO REPOSITORY}/MetaMobilePicker/test/test_reads_R1.fastq mmp_data
cp {PATH TO REPOSITORY}/MetaMobilePicker/test/test_reads_R2.fastq mmp_data
mkdir mmp_test_output # Our output files will go here
```
Now we can create our samples.txt file to look like this
```
testsample,mmp_data/test_reads_R1.fastq,mmp_data/test_reads_R2.fastq
```
Save this file as samples.txt for now. Next, we generate the config file:
```
metamobilepicker config --samples samples.txt --output test_config.yaml --outdir mmp_test_output
```
This should give you a file in the current directory called test_config.yaml that contains all the information we need to run MetaMobilePicker. 
Next, to test the installation of the pipeline, run the following command:
```
metamobilepicker run -c test_config.yaml --dryrun
```
If this doesn't give errors, go ahead and run 
```
metamobilepicker run -c test_config.yaml
```


### Output data
After running MetaMobilePicker, all results will be in the mmp_test_output directory we specified in the config file.
Your directory should look something like this:
```
$ ls mmp_test_output
docs    testsample  testsample_ATLAS_tmp    processed_fastq
```
The end results are in the testsample directory. Within this directory, there's subdirectories for the annotation part, the ATLAS QC part, the MetaSPAdes assembly and the (most importantly) MGE identification part.
Within the MGEs directory, the most elaborate output file is `testsample_MetaMobilePicker.out`. Opening this file should show something like this:
```
contig: 
    contig_id: NODE_1_length_17965_cov_31.138526
    length: 17965
    number_IS: 0
    number_annotations: 0
    phage: 
        score: 0.986850917339325

contig: 
    contig_id: NODE_2_length_12828_cov_50.403664
    length: 12828
    number_IS: 2
    number_annotations: 6
    plasmid:
        ID: plasmid
        score: 0.966940194363968
    Annotation_1:
        name: Drugs:Glycopeptides:VanA-type_regulator:VANRA
        start: 235
        stop: 720
        source: megares
        gene: VANRA
        accession: MEG_7452
    Annotation_2:
        name: Drugs:Glycopeptides:VanA-type_regulator:VANRA
        start: 873
        stop: 1783
        source: megares
        gene: VANRA
        accession: MEG_7458
    IS_1:
        ID: IS6_292
        class: IS6
        start: 2107
        stop: 2921
    Annotation_3:
        name: VanHAX
        start: 2944
        stop: 5549
        source: resfinder
        gene: VanHAX_2
        accession: M97297
    Annotation_4:
        name: Drugs:Glycopeptides:VanA-type_resistance_protein:VANHA
        start: 2944
        stop: 5549
        source: megares
        gene: VANHA
        accession: MEG_7410
    IS_2:
        ID: ISL3_126|ISL3||protein:plasmid:139408
        class: ISL3
        start: 5744
        stop: 7264
    Annotation_5:
        name: Drugs:Glycopeptides:VanA-type_regulator:VANRA
        start: 7271
        stop: 8426
        source: megares
        gene: VANRA
        accession: MEG_7454
    Annotation_6:
        name: Drugs:Glycopeptides:VanA-type_regulator:VANRA
        start: 8404
        stop: 9099
        source: megares
        gene: VANRA
        accession: MEG_7453
```
This shows we found two contigs containing one or more MGE. The first contig is predicted to be a phage, the second is predicted as a plasmid. Additionally, the second contig contains two insertion sequences and six ARGs.

This file has information on all contigs containing at least one MGE. The file is according to the YML format and can be parsed as such. Additionally (soon) a fasta file containing only those contigs containing MGEs is provided in the results folder. 

### Snakemake profiles
If you have an HPC available, install a snakemake profile for your specific job submission software, and run the following command to have snakemake submit the jobs to the HPC:
```
metamobilepicker run --profile [name of slurm profile]
``` 

### Other options
In the events of a failing pipeline, the directory can get locked. Use the following command before retrying:
```
metamobilepicker run --unlock
```
If you wish to run the pipeline with more cores than the default of 1 without running on an HPC, you can add them using the '--cores [cores]' option.

## Visual overview

<img src="workflow.png" width="100%" style="display: block; margin: auto;" />




